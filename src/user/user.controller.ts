import { NextFunction, Router, Request, Response } from 'express'
import Controller from '../interfaces/controller.interface'
import validationMiddleware from '../middleware/validation.middleware'
import UserDto from '../user/user.dto'
import UserService from './user.service'
import UserNotFoundException from '../exceptions/UserNotFoundException'
import TokenData from '../interfaces/tokenData.interface'
import AuthService from '../auth/auth.service'
import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException'

class UserController implements Controller {
  public path = '/user'
  public router = Router()
  public userService = new UserService()
  public authService = new AuthService()

  constructor() {
    this.initializeRoutes()
  }

  private initializeRoutes() {
    this.router.patch(
      `${this.path}/:id`,
      validationMiddleware(UserDto, true),
      this.updateUser
    )
  }

  private updateUser = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const { token, ...userData }: TokenData & UserDto = request.body
    const userId = request.params.id

    try {
      const user = await this.authService.verifyToken({ token })
      if (user.id !== parseInt(request.params.id)) {
        throw new WrongAuthenticationTokenException()
      }
    } catch (err) {
      return next(err)
    }

    try {
      const res = await this.userService.update(parseInt(userId), userData)
      response.send(res)
    } catch (err) {
      next(err)
    }
  }
}

export default UserController
