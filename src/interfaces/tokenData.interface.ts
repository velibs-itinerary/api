interface TokenData {
  token: string
}

interface TokenPayload {
  iss: number
  iat: number
  exp: number
}

export default TokenData
export { TokenPayload }
