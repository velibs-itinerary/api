# Suppression du fichier environnement
if (Test-Path -Path ".env") {
    Remove-Item -Path ".env" -Force
}

# Remplissage du fichier environnement example
'DATABASE_URL="file:./dev.db"' | Set-Content -Path ".env.example"

# Création du nouveau fichier environnement
Copy-Item -Path ".env.example" -Destination ".env"

# Suppression du dossier de migrations
if (Test-Path -Path "./prisma/migrations") {
    Remove-Item -Path "./prisma/migrations" -Force -Recurse
}

# Génération des nouvelles migrations
(Get-Content -Path "./prisma/schema.prisma") -replace "postgres", "sqlite" | Set-Content -Path "./prisma/schema.prisma"
npx prisma migrate dev -n "init"
npx prisma generate