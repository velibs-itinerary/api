<p align="center">
<img src="https://pngimg.com/uploads/bicycle/bicycle_PNG5374.png" width="80px">
</p>
<h1 align="center">Velibs Itinerary - Authentication API</h1>

<div align="center">

[![NodeJS](https://img.shields.io/badge/nodejs-20.3-009D28.svg?style=for-the-badge&logo=node.js)](https://nodejs.org/)
[![Docker](https://img.shields.io/badge/docker-24.0.2-0DB7ED.svg?style=for-the-badge&logo=docker)](https://docker.com/)
[![Docker Compose](https://img.shields.io/badge/docker_compose-2.19.1-384D54.svg?style=for-the-badge&logo=docker)](https://docker.com/)
[![Postgres](https://img.shields.io/badge/postgresql-15.3-336791.svg?style=for-the-badge&logo=postgresql)](https://www.postgresql.org/)

</div>

---

## 📝 Table des matières

- [📝 Table des matières](#📝-table-des-matières)
- [📚 Présentation du projet](#📚-présentation-du-projet)
- [⚙️ Setup le projet](#⚙️-setup-le-projet)
- [🔀 Passer à SQLite, sans docker](#🔀-passer-à-sqlite-sans-docker)
- [👨‍👦‍👦 Participants](#👨‍👦‍👦-participants)

## 📚 Présentation du projet

Le projet Velibs Itinerary à été développé pour le module `API et Services web` proposé par `Livecampus`.

Ici le projet visait le système d'authentification. Il était demandé de fournir les méthodes suivantes :

- Inscription
- Connexion
- Déconnexion
- Validation du JWT
- Changement des informations

Nous avons donc décidé de partir sur un projet rédigé en `typescript`, grâce à `nodejs` et `express`. Le SGBD choisit est `postgresql`, et nous avons décidé d'utiliser `docker` pour mettre en place le projet et le lancer simplement par la suite.

## ⚙️ Setup le projet

Si cette étape ne fonctionne pas à quelconque étape du lancement, merci de vous référer à [🔀 Passer à SQLite, sans docker](#🔀-passer-à-sqlite-sans-docker).

Toutes les commandes réalisés ci-dessous sont spécifiés dans le package.json.

**Lancement du projet**

- Copier le `.env.example` en `.env`, ne modifier son contenu que si nécessaire
- Lancer le projet et la base de données : `npm run dev`
- Lancer un seeding pour générer un utilisateur: `npm run seed`
- Lancer prisma studio pour accéder aux données de la base: `npm run studio`

**Arrêt du projet**

- Couper le docker : ⌨️ `ctrl+c`
- Supprimer le docker : `npm run stop`

## 🔀 Passer à SQLite, sans docker

Si le docker ne fonctionne pas, il reste la solution de repasser à `SQLite`, sans docker. Pour ça il suffit de lancer les commandes ci-dessous.

- Installer les dépendances: `pnpm install` ou `npm install`

⚠️ Attention veuillez suivre les instructions concernant votre système d'exploitation

**Windows Powershell**

Sous windows, il est nécessaire d'autoriser le système à lancer un script powershell avant de pouvoir lancer le suivant. Pour ceci il faut dans la plupart des cas exécuter:

- `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser`
Si vous ne pouvez pas le changer, l'administrateur de votre système as donc restreint l'utilisation des scripts powershell.

Ensuite vous pourrez donc :

- Migrer le projet sous SQLite: `scripts\migrate_sqlite.ps1`

**Unix**

- Migrer le projet sous SQLite: `chmod +x ./scripts/migrate_sqlite.sh && ./scripts/migrate_sqlite.sh`

**Tous les systèmes**

- Lancer le projet: `npm run dev:api`
- Lancer un seeding pour générer les utilisateurs: `npm run dev:seed`
- Lancer prisma studio pour accéder aux données de la base: `npm run dev:studio`

## 👨‍👦‍👦 Participants

**Mickaël Lebas**, **Alexis Py**, **Théo Posty**
