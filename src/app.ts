import express from 'express'
import bodyParser from 'body-parser'
import Controller from './interfaces/controller.interface'
import morgan from 'morgan'
import swaggerUi from 'swagger-ui-express'
import errorMiddleware from './middleware/error.middleware'
import cors from 'cors';

const PORT = process.env.port || 8000

class App {
  public app: express.Application

  constructor(controllers: Controller[]) {
    this.app = express()

    this.initializeMiddlewares()
    this.initializeControllers(controllers)
    this.initializeErrorHandling()
  }

  public listen() {
    this.app.listen(PORT, () => {
      console.log(`App listening on the port ${PORT}`)
    })
  }

  public getServer() {
    return this.app
  }

  private initializeMiddlewares() {
    this.app.use(cors())
    this.app.use(bodyParser.json())
    this.app.use(morgan('tiny'))
    this.app.use(express.static('public'))
    this.app.use(
      '/docs',
      swaggerUi.serve,
      swaggerUi.setup(undefined, {
        swaggerOptions: {
          url: '/swagger.json',
        },
      })
    )
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach(controller => {
      this.app.use('/', controller.router)
    })
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware)
  }
}

export default App
