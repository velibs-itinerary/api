import { IsString, MinLength } from 'class-validator'

class UserDto {
  @IsString()
  @MinLength(6)
  public username: string

  @IsString()
  @MinLength(6)
  public password: string
}

export default UserDto
