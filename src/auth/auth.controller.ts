import { NextFunction, Router, Request, Response } from 'express'
import Controller from '../interfaces/controller.interface'
import AuthService from './auth.service'
import validationMiddleware from '../middleware/validation.middleware'
import UserDto from '../user/user.dto'
import TokenData from '../interfaces/tokenData.interface'

class AuthController implements Controller {
  public path = '/auth'
  public router = Router()
  public authService = new AuthService()

  constructor() {
    this.initializeRoutes()
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}/register`,
      validationMiddleware(UserDto),
      this.registration
    )
    this.router.post(
      `${this.path}/login`,
      validationMiddleware(UserDto),
      this.login
    )
    this.router.post(`${this.path}/logout`, this.logout)
    this.router.post(`${this.path}/verify`, this.verifyToken)
  }

  private registration = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const userData: UserDto = request.body
    try {
      const res = await this.authService.register(userData)
      response.send(res)
    } catch (err) {
      next(err)
    }
  }

  private login = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const userData: UserDto = request.body
    try {
      const res = await this.authService.login(userData)
      response.send(res)
    } catch (err) {
      next(err)
    }
  }

  private logout = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const tokenData: TokenData = request.body
    try {
      const res = await this.authService.logout(tokenData)
      response.send(res)
    } catch (err) {
      next(err)
    }
  }

  private verifyToken = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const tokenData: TokenData = request.body
    try {
      const res = await this.authService.verifyToken(tokenData)
      response.send(res)
    } catch (err) {
      next(err)
    }
  }
}

export default AuthController
