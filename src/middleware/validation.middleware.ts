import { plainToInstance } from 'class-transformer'
import { ValidationError, validate } from 'class-validator'
import { RequestHandler } from 'express'
import HttpException from '../exceptions/HttpException'

function validationMiddleware(
  type: any,
  skipMissingProperties = false
): RequestHandler {
  return (req, _res, next) => {
    validate(plainToInstance(type, req.body), { skipMissingProperties }).then(
      (errors: ValidationError[]) => {
        if (errors.length > 0) {
          const message = errors
            .map(err => Object.values(err.constraints || {}))
            .join(', ')
          next(new HttpException(400, message))
        } else {
          next()
        }
      }
    )
  }
}

export default validationMiddleware
