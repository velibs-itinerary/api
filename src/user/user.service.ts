import { SALT } from '../auth/auth.service'
import prisma from '../db'
import UserNotFoundException from '../exceptions/UserNotFoundException'
import UserDto from './user.dto'
import bcrypt from 'bcrypt'

class UserService {
  public user = prisma.user

  public async update(userId: number, userData: UserDto) {
    try {
      if (userData.password) {
        userData.password = await bcrypt.hash(userData.password, SALT)
      }

      await this.user.update({ where: { id: userId }, data: userData })
    } catch {
      throw new UserNotFoundException(`${userId}`)
    }
  }
}

export default UserService
