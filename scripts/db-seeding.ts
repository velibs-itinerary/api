import { PrismaClient } from '@prisma/client'
import { faker } from '@faker-js/faker'
import bcrypt from 'bcrypt'
import 'dotenv/config'

async function main() {
  const SALT = process.env.PASSWORD_SALT || 10

  const prisma = new PrismaClient()

  console.log('Running SQL seed...')

  const username = faker.internet.userName()
  const password = faker.internet.password()

  const hashedPassword = await bcrypt.hash(password, SALT)

  await prisma.user.create({
    data: {
      username,
      password: hashedPassword,
    },
  })

  console.log('User created !')
  console.log('You can connect now using :')
  console.log(`     Username: ${username}`)
  console.log(`     Password: ${password}`)
}

main()
