import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import prisma from '../db'
import UserWithThatUsernameAlreadyExistsException from '../exceptions/UserWithThatUsernameAlreadyExistsException'
import UserDto from '../user/user.dto'
import { User } from '@prisma/client'
import TokenData, { TokenPayload } from '../interfaces/tokenData.interface'
import WrongCredentialsException from '../exceptions/WrongCredentialsException'
import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException'

export const SALT = process.env.PASSWORD_SALT || 10
const JWT_SECRET =
  process.env.JWT_SECRET ||
  'some_default_not_secured_jwt_secret_please_add_JWT_SECRET_env_variable_to_define_a_new_one'

class AuthService {
  public user = prisma.user
  public validTokens = prisma.validTokens

  public async register(userData: UserDto) {
    if (
      await this.user.findUnique({
        where: {
          username: userData.username,
        },
      })
    ) {
      throw new UserWithThatUsernameAlreadyExistsException(userData.username)
    }

    const hashedPassword = await bcrypt.hash(userData.password, SALT)
    const newUser = await this.user.create({
      data: {
        ...userData,
        password: hashedPassword,
      },
    })

    const tokenData = this.createToken(newUser)

    await this.validTokens.create({
      data: {
        userId: newUser.id,
        token: tokenData.token,
      },
    })

    return {
      token: tokenData.token,
    }
  }

  public async login(userData: UserDto) {
    const user = await this.user.findUnique({
      where: { username: userData.username },
    })

    if (!user) throw new WrongCredentialsException()

    const isPasswordMatching = await bcrypt.compare(
      userData.password,
      user.password
    )

    if (!isPasswordMatching) throw new WrongCredentialsException()

    const tokenData = this.createToken(user)

    await this.validTokens.create({
      data: {
        userId: user.id,
        token: tokenData.token,
      },
    })

    return {
      token: tokenData.token,
    }
  }

  public async logout(tokenData: TokenData) {
    try {
      await this.validTokens.delete({
        where: {
          token: tokenData.token,
        },
      })
    } catch {
      throw new WrongAuthenticationTokenException()
    }
  }

  public async verifyToken(tokenData: TokenData) {
    let decodedToken
    try {
      decodedToken = jwt.verify(
        tokenData.token,
        JWT_SECRET
      ) as any as TokenPayload
    } catch (_e) {
      throw new WrongAuthenticationTokenException()
    }

    const foundUser = await this.user.findUnique({
      where: {
        id: decodedToken.iss,
      },
    })

    if (!foundUser) throw new WrongAuthenticationTokenException()

    const foundToken = await this.validTokens.findUnique({
      where: {
        token: tokenData.token,
      },
    })

    if (!foundToken) throw new WrongAuthenticationTokenException()

    const { password, ...user } = foundUser

    return user
  }

  private createToken(user: User): TokenData {
    const currentTimestamp = Date.now() / 1000 // in seconds
    const expiresIn = 60 * 60 * 24 * 7 // one week

    return {
      token: jwt.sign(
        {
          iss: user.id,
          iat: currentTimestamp,
        },
        JWT_SECRET,
        { expiresIn }
      ),
    }
  }
}

export default AuthService
