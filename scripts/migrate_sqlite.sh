#!/bin/bash

# Suppression du fichier environnement
rm -f .env

# Remplissage du fichier environnement example
echo "DATABASE_URL=\"file:./dev.db\"" > .env.example

# Création du nouveau fichier environnement
cp .env.example .env

# Suppression du dossier de migrations
rm -rf ./prisma/migrations

# Génération des nouvelles migrations
sed -i "s/postgres/sqlite/" ./prisma/schema.prisma
npx prisma migrate dev -n "init"
npx prisma generate